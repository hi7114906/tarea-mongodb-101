# Tarea | Mongodb 101

Vamos a utilizar una colección de calificaciones de estudiantes y con esto vamos a hacer consultas
en mongodb con un archivo llamado grades.json que es donde se encuentran las calificaciones de los
estudiantes. 

## Requisitos

- mongodb instalado o dockerizado.
- Archivo 'grades.json' para importar datos.

## Instalación

- Clona el repositorio de la siguiente manera:
    git clone git@gitlab.com:hi7114906/tarea-mongodb-101.git

- Asegurate de tener mongodb instalado o dockerizado.

## Uso

- Descargamos la imagen de la siguiente forma:
    docker pull mongo

- Para poner en uso el contenedor (NOTA: Importante poner la ruta en donde tienes el archivo grades.json) y se hace de la siguiente manera: 
    docker run -d --name mongodb -d -p 27017:27017 -v /ruta/local/hacia/grades.json:/data/db/grades.json mongo

- Para entrar a mongo se debe escribir lo siguiente:
    docker exec -it mongo-container bash 

- Enseguida se importaran los datos con el siguinete comando: 
    mongoimport -d students -c grades </data/db/grades.json

- En la parte donde nos encontramos escribiremos lo siguiente:
    mongosh

- Nos dira que estamos en un test y le daremos lo siguiente:
    use students

- Apartir de ahí puedes hacer las consultas que quieras
 

## Autor

Zaid Joel González Mendoza 353254

## Docente

Ing. Luis Antonio Ramírez Martínez

