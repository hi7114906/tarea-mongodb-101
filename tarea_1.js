// Tarea | Mongodb 101
// Zaid Joel González Mendoza 353254

/*
    1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando:
            $ mongoimport -d students -c grades < grades.json
*/

/* 
    2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la 
    colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros 
    arrojo el comando count?
            $ mongosh

            Current Mongosh Log ID:	6528362fbbba35c097ca8c10
            Connecting to:		mongodb://127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+2.0.1
            Using MongoDB:		7.0.2
            Using Mongosh:		2.0.1

            For mongosh info see: https://docs.mongodb.com/mongodb-shell/


            To help improve our products, anonymous usage data is collected and sent to MongoDB periodically (https://www.mongodb.com/legal/privacy-policy).
            You can opt-out by running the disableTelemetry() command.

            ------
            The server generated these startup warnings when booting
            2023-10-12T17:58:59.221+00:00: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine. See http://dochub.mongodb.org/core/prodnotes-filesystem
            2023-10-12T17:59:00.288+00:00: Access control is not enabled for the database. Read and write access to data and configuration is unrestricted
            2023-10-12T17:59:00.288+00:00: vm.max_map_count is too low
            ------

            test> use students
            switched to db students
            students> db.grades.count()
            DeprecationWarning: Collection.count() is deprecated. Use countDocuments or estimatedDocumentCount.
            800
*/

/*
    3) Encuentra todas las calificaciones del estudiante con el id numero 4.
            students> db.grades.find({student_id:4});
            [
            {
                _id: ObjectId("50906d7fa3c412bb040eb587"),
                student_id: 4,
                type: 'exam',
                score: 87.89071881934647
            },
            {
                _id: ObjectId("50906d7fa3c412bb040eb588"),
                student_id: 4,
                type: 'quiz',
                score: 27.29006335059361
            },
            {
                _id: ObjectId("50906d7fa3c412bb040eb58a"),
                student_id: 4,
                type: 'homework',
                score: 28.656451042441
            },
            {
                _id: ObjectId("50906d7fa3c412bb040eb589"),
                student_id: 4,
                type: 'homework',
                score: 5.244452510818443
            }
            ]
*/

/*
    4) ¿Cuántos registros hay de tipo exam? 
            students> db.grades.find({type:'exam'}).count();
            200
*/ 

/*
    5) ¿Cuántos registros hay de tipo homework?
            students> db.grades.find({type:'homework'}).count();
            400
*/ 

/*
    6) ¿Cuántos registros hay de tipo quiz?
            db.grades.find({type:'quiz'}).count();
            200
*/

/*
    7) Elimina todas las calificaciones del estudiante con el id numero 3
            students> db.grades.deleteMany({student_id:3});
            { acknowledged: true, deletedCount: 4 }
*/

/*
    8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
            students> db.grades.find({score: 75.29561445722392}).count();
            1
*/

/*
    9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
            students> db.grades.update({_id:ObjectId("50906d7fa3c412bb040eb591")}, {$set:{"score":100}});
            {
            acknowledged: true,
            insertedId: null,
            matchedCount: 1,
            modifiedCount: 1,
            upsertedCount: 0
            }
*/

/*
    10) A qué estudiante pertenece esta calificación.
            students> db.grades.find({score:100});
            [
            {
                _id: ObjectId("50906d7fa3c412bb040eb591"),
                student_id: 6,
                type: 'homework',
                score: 100
            }
            ]
*/